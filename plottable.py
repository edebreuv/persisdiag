# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

from typing import Optional, Tuple, Union


class __plottable_t__:
    #
    __slots__ = ("birth_label", "birth", "death", "children", "h_pos")

    def __init__(self, node):
        #
        self.birth_label = node.birth_label
        self.birth = node.birth
        self.death = node.death

        self.children = None
        self.h_pos = None

    def Plot(self, axes) -> Tuple[int, int]:
        #
        PlotEdges(self, axes)
        return PlotNodes(self, axes)


class plottable_tree_t(__plottable_t__):
    #
    # Adapted from a Bill Mill's code for Buchheim's method:
    #     http://github.com/llimllib
    #     http://llimllib.github.io/pymag-trees/
    #     http://github.com/llimllib/pymag-trees/
    #     http://github.com/llimllib/pymag-trees/blob/master/buchheim.py
    #
    __slots__ = (
        "ancestor",
        "parent",
        "thread",
        "mod",
        "change",
        "shift",
        "sibling_idx",
        "__lmost_sibling__",
    )

    def __init__(self, node, parent: plottable_tree_t = None, sibling_idx: int = 1):
        #
        super().__init__(node)

        self.ancestor = self
        self.parent = parent
        if node.children is None:
            self.children = []
        else:
            self.children = [
                plottable_tree_t(child, self, idx)
                for idx, child in enumerate(node.children, start=1)
            ]

        self.h_pos = -1.0

        self.thread = None
        self.mod = 0
        self.change = 0
        self.shift = 0
        self.sibling_idx = sibling_idx
        self.__lmost_sibling__ = None

    @property
    def left(self):
        #
        return self.thread or len(self.children) and self.children[0]

    @property
    def right(self):
        #
        return self.thread or len(self.children) and self.children[-1]

    @property
    def left_brother(self) -> Optional[plottable_tree_t]:
        #
        result = None

        if self.parent:
            for brother in self.parent.children:
                if brother == self:
                    return result
                else:
                    result = brother

        return result

    @property
    def lmost_sibling(self) -> plottable_tree_t:
        #
        if (
            not self.__lmost_sibling__
            and self.parent
            and self != self.parent.children[0]
        ):
            self.__lmost_sibling__ = self.parent.children[0]

        return self.__lmost_sibling__

    def Plot(self, axes) -> Tuple[int, int]:
        #
        __FirstPass__(self)
        min_h_pos = __SecondPass__(self)
        if min_h_pos < 0.0:
            __ThirdPass__(self, -min_h_pos)

        return super().Plot(axes)


class plottable_root_t(__plottable_t__):
    #
    h_pos: float = 0.0

    def __init__(self, node):
        #
        super().__init__(node)

        if node.children is None:
            self.children = []
            self.h_pos = plottable_root_t.h_pos
            plottable_root_t.h_pos += 1
        else:
            self.children = [plottable_root_t(child) for child in node.children]
            self.h_pos = (
                sum(child.h_pos for child in self.children) / self.children.__len__()
            )


plottable_h = Union[__plottable_t__, plottable_tree_t]


def PlotNodes(node: plottable_h, axes) -> Tuple[int, int]:
    #
    node_label = f" C{node.birth_label}[{node.birth}..{node.death}]"
    axes.plot(node.h_pos, node.birth, "bx")
    axes.text(node.h_pos, node.birth, node_label)

    min_birth = max_birth = node.birth

    for child in node.children:
        min_birth_l, max_birth_l = PlotNodes(child, axes)
        min_birth = min(min_birth, min_birth_l)
        max_birth = max(max_birth, max_birth_l)

    return min_birth, max_birth


def PlotEdges(node: plottable_h, axes) -> None:
    #
    for child in node.children:
        if isinstance(node, plottable_root_t) or (
            (node.parent is not None) and (node.parent.birth == node.parent.death)
        ):
            axes.plot((node.h_pos, child.h_pos), (node.birth, child.birth), "g-")
        else:
            axes.plot(
                (node.h_pos, node.h_pos, child.h_pos),
                (node.birth, node.death, child.birth),
                "g-",
            )

        PlotEdges(child, axes)


def __FirstPass__(node: plottable_tree_t, distance: float = 1.0) -> None:
    #
    if len(node.children) == 0:
        if node.lmost_sibling:
            node.h_pos = node.left_brother.h_pos + distance
        else:
            node.h_pos = 0.0
    else:
        default_ancestor = node.children[0]
        for child in node.children:
            __FirstPass__(child)
            default_ancestor = __Apportion__(child, default_ancestor, distance)
        __ApplyShifts__(node)

        midpoint = (node.children[0].h_pos + node.children[-1].h_pos) / 2.0

        left_brother = node.left_brother
        if left_brother:
            node.h_pos = left_brother.h_pos + distance
            node.mod = node.h_pos - midpoint
        else:
            node.h_pos = midpoint


def __SecondPass__(
    node: plottable_tree_t, mod: int = 0, min_h_pos: float = None
) -> float:
    #
    node.h_pos += mod

    if min_h_pos is None or node.h_pos < min_h_pos:
        min_h_pos = node.h_pos

    for child in node.children:
        min_h_pos = __SecondPass__(child, mod + node.mod, min_h_pos)

    return min_h_pos


def __ThirdPass__(node: plottable_tree_t, h_shift: float) -> None:
    #
    node.h_pos += h_shift
    for child in node.children:
        __ThirdPass__(child, h_shift)


def __Apportion__(
    node: plottable_tree_t, default_ancestor: plottable_tree_t, distance: float
) -> plottable_tree_t:
    #
    left_brother = node.left_brother
    if left_brother is not None:
        # In buchheim notation:
        #     i == inner; o == outer; r == right; l == left; r = +; l = -
        vir = vor = node
        vil = left_brother
        vol = node.lmost_sibling
        sir = sor = node.mod
        sil = vil.mod
        sol = vol.mod
        while vil.right and vir.left:
            vil = vil.right
            vir = vir.left
            vol = vol.left
            vor = vor.right
            vor.ancestor = node
            shift = (vil.h_pos + sil) - (vir.h_pos + sir) + distance
            if shift > 0.0:
                __MoveSubtree__(__Ancestor__(vil, node, default_ancestor), node, shift)
                sir = sir + shift
                sor = sor + shift
            sil += vil.mod
            sir += vir.mod
            sol += vol.mod
            sor += vor.mod
        if vil.right and not vor.right:
            vor.thread = vil.right
            vor.mod += sil - sor
        else:
            if vir.left and not vol.left:
                vol.thread = vir.left
                vol.mod += sir - sol
            default_ancestor = node

    return default_ancestor


def __Ancestor__(
    vil: plottable_tree_t, node: plottable_tree_t, default_ancestor: plottable_tree_t
) -> plottable_tree_t:
    #
    # The relevant text is at the bottom of page 7 of
    # "Improving Walker's Algorithm to Run in Linear Time" by Buchheim et al, (2002)
    # http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.16.8757&rep=rep1&type=pdf
    #
    if vil.ancestor in node.parent.children:
        return vil.ancestor
    else:
        return default_ancestor


def __ApplyShifts__(node: plottable_tree_t) -> None:
    #
    shift = change = 0
    for child in node.children[::-1]:
        child.h_pos += shift
        child.mod += shift
        change += child.change
        shift += child.shift + change


def __MoveSubtree__(wl: plottable_tree_t, wr: plottable_tree_t, shift: float) -> None:
    #
    subtrees = wr.sibling_idx - wl.sibling_idx
    wr.change -= shift / subtrees
    wr.shift += shift
    wl.change += shift / subtrees
    wr.h_pos += shift
    wr.mod += shift

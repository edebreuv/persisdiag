# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from c_forest import c_forest_t, which_cmp_e
import filtration as fl_
import test_common as tc_

import numpy as np_
import skimage as im_


array_t = np_.ndarray


def test(image: array_t, min_life_time: int, level_step: int) -> None:
    #
    filtration_fct = fl_.Thresholding

    c_forest = c_forest_t.FromDecreasingSizeData(
        image, filtration_fct, level_step=level_step
    )
    llt_amap = c_forest.MapOfCmpsWithLongLifeTime(
        image, filtration_fct, True, min_life_time=min_life_time, which=which_cmp_e.ALL
    )
    llt_omap = c_forest.MapOfCmpsWithLongLifeTime(
        image,
        filtration_fct,
        True,
        min_life_time=min_life_time,
        which=which_cmp_e.OUTER,
    )
    llt_imap = c_forest.MapOfCmpsWithLongLifeTime(
        image,
        filtration_fct,
        True,
        min_life_time=min_life_time,
        mark_with="life_time",
        which=which_cmp_e.INNER,
    )

    c_forest.Plot()
    tc_.test_final_plots(image, filtration_fct, c_forest, llt_amap, llt_omap, llt_imap)


if __name__ == "__main__":
    #
    import sys as sy_

    min_life_time_ = 1
    level_step_ = 1

    if sy_.argv.__len__() > 1:
        name_ = sy_.argv[1]
        if sy_.argv.__len__() > 2:
            try:
                min_life_time_ = int(sy_.argv[2])
            except ValueError:
                print(
                    f"{sy_.argv[2]}: Second argument must be an integer (Minimum life time)"
                )
                sy_.exit(-1)
        level_step_ = 15

        image_ = None
        try:
            image_ = im_.data.load(name_ + ".png")
        except FileNotFoundError:
            print(
                f"{name_}: Not a Scikit-image image in PNG format\n"
                f"    Valid image names can be found in 'data' module documentation at\n"
                f"    https://scikit-image.org/docs/dev/api/skimage.data.html?highlight=data#module-skimage.data"
            )
            sy_.exit(-1)
        if image_.shape.__len__() == 3:
            image_ = im_.color.rgb2gray(image_)
        image_ = im_.filters.gaussian(image_, sigma=3, multichannel=False)
        image_ = np_.around(
            255.0 * (image_ - image_.min()) / (image_.max() - image_.min())
        ).astype(np_.uint8)
    else:
        name_ = "peaks.png"
        image_ = im_.io.imread(name_)

    test(image_, min_life_time_, level_step_)

# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

from typing import List, Tuple


class component_t:
    #
    level_step: int = 1

    __slots__ = (
        "birth_label",
        "birth",
        "death",
        "parent",
        "children",
        "__latest_lbl__",
    )

    def __init__(self, level: int, label: int, parent: component_t = None):
        #
        self.birth_label = label
        self.birth = level
        self.death = level
        self.parent = parent
        self.children = None
        self.__latest_lbl__ = label

    @property
    def uid(self) -> Tuple[int, int]:
        #
        return self.birth, self.birth_label

    @property
    def life_time(self) -> int:
        #
        return self.death - self.birth + component_t.level_step

    @property
    def is_origin(self) -> bool:
        #
        return self.parent is None

    @property
    def n_children(self) -> int:
        #
        if self.children is None:
            return 0
        return self.children.__len__()

    def __str__(self) -> str:
        #
        has_no_parent = self.parent is None
        has_no_children = self.children is None

        if has_no_parent:
            parent = ""
        else:
            parent = f"<L{self.parent.birth}.C{self.parent.birth_label}"
        if has_no_children:
            children = ""
        else:
            children = f">{self.children.__len__()}"
        if has_no_parent or has_no_children:
            sep_1 = ""
        else:
            sep_1 = "/"
        if has_no_parent and has_no_children:
            sep_2 = ""
        else:
            sep_2 = " "

        return f"L{self.birth}.C{self.birth_label}: {parent}{sep_1}{children}{sep_2}[{self.birth}..{self.death}]"

    def AllDescendants(
        self, with_level: bool = False, descendance_lvl: int = 0
    ) -> List[Tuple[component_t, int]]:
        #
        # Normally, descendance_lvl will not be set by the initial caller. It could be if calling the method from
        # a non-origin component whose descendance level is known.
        #
        if with_level:
            result = [(self, descendance_lvl)]
        else:
            result = [self]

        if self.children is not None:
            for child in self.children:
                result.extend(
                    child.AllDescendants(
                        with_level=with_level, descendance_lvl=descendance_lvl + 1
                    )
                )

        return result

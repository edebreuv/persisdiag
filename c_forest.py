# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

from plottable import plottable_root_t, plottable_tree_t
from component import component_t
from filtration import filtration_h

import itertools as it_
from enum import Enum as enum_t
from enum import auto as AutoIdx
from typing import Dict, Tuple

import matplotlib.pyplot as pl_
import numpy as np_


array_t = np_.ndarray


class which_cmp_e(enum_t):
    #
    ALL = AutoIdx()
    OUTER = AutoIdx()
    INNER = AutoIdx()


class c_forest_t(list):
    #
    # Component forest (made up of components trees)
    #
    __slots__ = ("level_step",)

    def __init__(self):
        #
        super().__init__()
        self.level_step = None

    @classmethod
    def FromDecreasingSizeData(
        cls,
        data: array_t,
        filtration_fct: filtration_h,
        stt_level: int = None,  # stt=start
        end_level: int = None,
        level_step: int = None,
    ) -> c_forest_t:
        #
        instance = cls()
        instance.level_step = level_step
        component_t.level_step = level_step

        if stt_level is None:
            stt_level = data.min(initial=None)
        if end_level is None:
            end_level = data.max(initial=None)
        if level_step is None:
            level_step = 1
        assert (
            (end_level - stt_level) / level_step
        ).is_integer(), f"{end_level} =/= {stt_level} + k*{level_step}"

        # prv=previous (where parent components are)
        slice_prv = filtration_fct(data, stt_level, True)
        instance.extend(
            component_t(stt_level, label)
            for label in range(1, slice_prv.max(initial=None) + 1)
        )
        parents = list(instance)  # list(instance): just to make a copy

        # nxt=next (where child components are)
        for level_nxt in range(stt_level + level_step, end_level + 1, level_step):
            slice_nxt = filtration_fct(data, level_nxt, True)

            # Leave 'tuple' here since parents is modified in the for-loop below
            parent_nfos = tuple((parent, parent.__latest_lbl__) for parent in parents)
            for component, cmp_lbl in parent_nfos:
                child_lbls = np_.unique(slice_nxt[slice_prv == cmp_lbl])
                # Remove the background label if present
                if child_lbls[0] == 0:
                    child_lbls = child_lbls[1:]

                if child_lbls.size > 1:
                    component.children = [
                        component_t(level_nxt, label, parent=component)
                        for label in child_lbls
                    ]
                    parents.remove(component)
                    parents.extend(component.children)
                elif child_lbls.size > 0:
                    component.__latest_lbl__ = child_lbls.item()
                    component.death = level_nxt
                else:
                    parents.remove(component)

            slice_prv = slice_nxt

        return instance

    @classmethod
    def FromIncreasingSizeData(
        cls, data: array_t, filtration_fct: filtration_h, n_iterations: int
    ) -> c_forest_t:
        #
        instance = cls()
        instance.level_step = 1
        component_t.level_step = 1

        # prv=previous (where child components are)
        slice_prv = filtration_fct(data, 0, True)
        children = tuple(
            component_t(0, label) for label in range(1, slice_prv.max(initial=None) + 1)
        )
        parents = ()  # Just in case the for-loop below does not loop

        # nxt=next (where parent components are)
        for level_nxt in range(1, n_iterations + 1):
            slice_nxt = filtration_fct(data, level_nxt, True)
            parents = []

            for cmp_lbl in range(1, slice_nxt.max(initial=None) + 1):
                child_lbls = np_.unique(slice_prv[slice_nxt == cmp_lbl])
                # Remove the background label if present
                if child_lbls[0] == 0:
                    child_lbls = child_lbls[1:]

                if child_lbls.size > 1:
                    component = component_t(level_nxt, cmp_lbl)
                    component.children = [
                        child
                        for child in children
                        if child.__latest_lbl__ in child_lbls
                    ]
                    for child in component.children:
                        child.death = level_nxt - 1
                    parents.append(component)
                else:
                    child = tuple(
                        child
                        for child in children
                        if child.__latest_lbl__ == child_lbls.item()
                    )[0]
                    child.__latest_lbl__ = cmp_lbl
                    parents.append(child)

            if parents.__len__() == 1:
                n_iterations = level_nxt
                break
            slice_prv, children = slice_nxt, parents

        for parent in parents:
            parent.death = n_iterations
        instance.extend(parents)

        return instance

    def LifeTimes(self) -> Dict[Tuple[int, int], int]:
        #
        result = []

        for origin in self:
            result.extend((cmp.uid, cmp.life_time) for cmp in origin.AllDescendants())
        result.sort(key=lambda cmp_nfo: -cmp_nfo[1])

        return dict(result)

    def MapOfCmpsWithLongLifeTime(
        self,
        data: array_t,
        filtration_fct: filtration_h,
        decreasing_size: bool,
        min_life_time: int = 1,
        mark_with: str = "birth",
        which: which_cmp_e = which_cmp_e.ALL,
    ) -> array_t:
        #
        result = np_.zeros_like(data, dtype=np_.int64)

        cmp_to_keep = []
        for origin in self:
            cmp_to_keep.extend(
                cmp for cmp in origin.AllDescendants() if cmp.life_time >= min_life_time
            )
        if (decreasing_size and (which == which_cmp_e.INNER)) or (
            (not decreasing_size) and (which == which_cmp_e.OUTER)
        ):
            reversing = -1.0
        else:
            reversing = 1.0
        cmp_to_keep.sort(key=lambda cmp: reversing * cmp.birth)

        for birth, components in it_.groupby(cmp_to_keep, key=lambda cmp: cmp.birth):
            slice_lbl = filtration_fct(data, birth, False)

            for component in components:
                cmp_indexing = slice_lbl == component.birth_label
                if which == which_cmp_e.ALL:
                    result[cmp_indexing] = getattr(component, mark_with)
                elif which == which_cmp_e.OUTER:
                    one_site = tuple(idc[0] for idc in cmp_indexing.nonzero())
                    if result[one_site] == 0:
                        result[cmp_indexing] = getattr(component, mark_with)
                elif (result[cmp_indexing] == 0).all():
                    result[cmp_indexing] = getattr(component, mark_with)

        return result

    def Plot(self, upside_down: bool = False) -> None:
        #
        n_origins = self.__len__()

        for origin in self:
            figure = pl_.figure()
            axes = figure.add_subplot()
            if n_origins > 1:
                axes.set_title(origin.__str__())

            if upside_down:
                plottable_hierarchy = plottable_root_t(origin)
            else:
                plottable_hierarchy = plottable_tree_t(origin)
            min_birth, max_birth = plottable_hierarchy.Plot(axes)

            axes.set_xticks(())
            axes.set_yticks(range(min_birth, max_birth + 1, self.level_step))
            axes.spines["right"].set_visible(False)
            axes.spines["top"].set_visible(False)
            axes.spines["bottom"].set_visible(False)

    def PlotLifeTimeDiagram(self) -> None:
        #
        figure = pl_.figure()
        axes = figure.add_subplot()

        min_birth = np_.iinfo(np_.int64).max
        max_birth = max_death = np_.iinfo(np_.int64).min
        for origin in self:
            for descendant in origin.AllDescendants():
                min_birth = min(min_birth, descendant.birth)
                max_birth = max(max_birth, descendant.birth)
                max_death = max(max_death, descendant.death)
                axes.plot(
                    (descendant.birth, descendant.birth),
                    (descendant.birth, descendant.death),
                    markeredgecolor="b",
                    marker="x",
                    color="g",
                    linestyle="-",
                )

        axes.plot((min_birth, max_birth), (min_birth, max_birth), "k:")
        axes.set_xticks(range(min_birth, max_birth + 1, self.level_step))
        axes.set_yticks(range(min_birth, max_death + 1, self.level_step))
        axes.spines["right"].set_visible(False)
        axes.spines["top"].set_visible(False)
        axes.get_xaxis().set_label_text("Birth Time")
        axes.get_yaxis().set_label_text("Death Time")
        axes.get_yaxis().grid(linestyle=":")

    def PlotLifeTimeBarCode(self) -> None:
        #
        life_times = tuple(self.LifeTimes().values())

        figure = pl_.figure()
        axes = figure.add_subplot()
        axes.barh(range(1, life_times.__len__() + 1), life_times)
        axes.set_xticks(range(min(life_times), max(life_times) + 1, self.level_step))
        axes.set_yticks(())
        axes.get_xaxis().grid(linestyle=":")

    def PlotFiltrations(
        self, data: array_t, filtration_fct: filtration_h, n_filtrations: int = 8
    ):
        #
        all_births = []
        for origin in self:
            all_births.extend(cmp.birth for cmp in origin.AllDescendants())
        all_births = sorted(list(set(all_births)))

        birth_idc = np_.linspace(0, all_births.__len__() - 1, n_filtrations)
        for i_idx in range(1, n_filtrations):
            index = np_.around(birth_idc[i_idx]).item()
            if index <= birth_idc[i_idx - 1]:
                index = birth_idc[i_idx - 1] + 1
            birth_idc[i_idx] = index

        for b_idx in it_.takewhile(lambda idx: idx < all_births.__len__(), birth_idc):
            b_idx = int(b_idx)
            figure = pl_.figure()
            axes = figure.add_subplot()
            axes.set_title(f"Birth={all_births[b_idx]}")
            axes.matshow(
                filtration_fct(data, all_births[b_idx], False) > 0, cmap="gray"
            )

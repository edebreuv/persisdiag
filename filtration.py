# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from typing import Callable, Union

import numpy as np_
import skimage.measure as ms_
import skimage.morphology as mp_


array_t = np_.ndarray
filtration_h = Callable[[array_t, Union[int, float], bool], array_t]


incremental_data_cache_g = {}


def Thresholding(data: array_t, threshold: Union[int, float], _: bool) -> array_t:
    #
    return ms_.label(data >= threshold)


def BinaryDilationFiltration(selem: array_t) -> filtration_h:
    #
    def BinaryDilation(
        data: array_t, n_dilations: Union[int, float], incremental: bool
    ) -> array_t:
        #
        global incremental_data_cache_g

        if incremental:
            cache_id = BinaryDilationFiltration.__name__ + id(data).__str__()
        else:
            cache_id = None

        if n_dilations == 0:
            dilated_data = data
        elif incremental:
            dilated_data = incremental_data_cache_g[cache_id]
            dilated_data = mp_.binary_dilation(dilated_data, selem=selem)
        else:
            dilated_data = data
            for _ in range(n_dilations):
                dilated_data = mp_.binary_dilation(dilated_data, selem=selem)

        if incremental:
            incremental_data_cache_g[cache_id] = dilated_data

        return ms_.label(dilated_data)

    return BinaryDilation
